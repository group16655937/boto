import boto3
import sys

ch= sys.argv[1]

aws_con=boto3.session.Session()
ec2_con_cli=aws_con.client(service_name="ec2",region_name="us-east-1")

if ch=="start":
    print("Starting ec2 instace...")
    ec2_con_cli.start_instances(InstanceIds=['i-0c7889ce3adf77aa6'])
    waiter=ec2_con_cli.get_waiter('instance_running')
    waiter.wait(InstanceIds=['i-0c7889ce3adf77aa6']) 
    print("Now your ec2 instace is up and running")
    
elif ch=="stop":
    print("Stopping ec2 instace...")
    ec2_con_cli.stop_instances(InstanceIds=['i-0c7889ce3adf77aa6'])
    waiter=ec2_con_cli.get_waiter('instance_stopped')
    waiter.wait(InstanceIds=['i-0c7889ce3adf77aa6']) 
    print("Now your ec2 instace stopped")
